﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using HTTPParser.Models;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Web.Helpers;

namespace HTTPParser.Controllers
{
    public class ParserController : Controller
    {
        //
        // GET: /Parser/
        public string Set()
        {
            NameValueCollection headers = Request.Headers;
            Dictionary<string, string> headersDictionary = new Dictionary<string, string>();
            for (int i = 0; i < headers.Count; i++)
            {
                headersDictionary.Add(headers.GetKey(i), headers.Get(i));
            }
            
            var serializer = new JavaScriptSerializer();
            var serializedResult = serializer.Serialize(headersDictionary);

//            Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(serializedResult);

            string body;
            using (Stream receiveStream = Request.InputStream)
            {
                using (StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8))
                {
                    body = readStream.ReadToEnd();
                }
            }

            using (DataEntities1 entities = new DataEntities1())
            {
                Datum data = new Datum();
                data.ID = Guid.NewGuid();
                data.Headers = serializedResult;
                data.Body = body;
                entities.Data.Add(data);
                entities.SaveChanges();
            }

            return "ok";
        }

        [HttpGet]
        public ActionResult Get()
        {
            List<Datum> all;
            using (DataEntities1 entities = new DataEntities1())
            {
                all = entities.Data.Where(d => d.ID != new Guid()).ToList();
            }

            ViewBag.all = all;

            return View();
        }

        [HttpGet]
        public string Clear()
        {
            using (DataEntities1 db = new DataEntities1())
            {
                var itemsToDelete = db.Data.Where(d => d.ID != Guid.Empty);
                db.Data.RemoveRange(itemsToDelete);
                db.SaveChanges();
            }

            return "ok";
        }

        [HttpGet]
        public string Test()
        {
            return "ok";
        }

    }
}
